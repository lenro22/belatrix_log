import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import enums.LevelLog;
import enums.LogLocation;
import model.Log;
import util.DataSourceFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DriverManager.class, DataSourceFactory.class, FileHandler.class, Connection.class, Statement.class })
public class JobLoggerTest {

	@Mock
	Connection mockConnection;
	@Mock
	Statement mockStatement;

	@Before
	public void init() throws Exception {
		initMocks(this);
		PowerMockito.mockStatic(DataSourceFactory.class);
		PowerMockito.mockStatic(Statement.class);
		PowerMockito.mockStatic(FileHandler.class);
		when(DataSourceFactory.getConnection()).thenReturn(mockConnection);
        when(DataSourceFactory.getConnection().createStatement()).thenReturn(mockStatement);
	}

	@Test
	public void logAsText() throws Exception {
		JobLogger jobLogic = new JobLogger();
		Log log = new Log("As text", Level.WARNING);
		Set<LogLocation> logLocations = new HashSet<LogLocation>();
		logLocations.add(LogLocation.TEXT);	
		Assert.assertEquals(true, jobLogic.logMessage(log, logLocations, LevelLog.WARNING.toString()));
	}
	
	@Test
	public void logAsDatabase() throws Exception {
		JobLogger jobLogic = new JobLogger();
		Log log = new Log("As database", Level.SEVERE);
		Set<LogLocation> logLocations = new HashSet<LogLocation>();
		logLocations.add(LogLocation.DATABASE);
		Assert.assertEquals(true,jobLogic.logMessage(log, logLocations, LevelLog.ERROR.toString()));
	}
	
	@Test
	public void logAsConsole() throws Exception {
		JobLogger jobLogic = new JobLogger();
		Log log = new Log("As console", Level.INFO);
		Set<LogLocation> logLocations = new HashSet<LogLocation>();
		logLocations.add(LogLocation.CONSOLE);
		Assert.assertEquals(true,jobLogic.logMessage(log, logLocations, LevelLog.INFO.toString()));
	}
	
	@Test(expected = Exception.class)
	public void notLocation() throws Exception {
		JobLogger jobLogic = new JobLogger();
		Log log = new Log("not location", Level.INFO);
		Set<LogLocation> logLocations = new HashSet<LogLocation>();
		Assert.assertEquals(false,jobLogic.logMessage(log, logLocations, LevelLog.INFO.toString()));
	}
}
