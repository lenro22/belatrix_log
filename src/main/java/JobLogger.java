import java.sql.Statement;
import java.text.DateFormat;
import java.util.Date;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import enums.LevelLog;
import enums.LogLocation;
import model.Log;
import util.DataSourceFactory;
import util.KeepFile;

public class JobLogger {

	private static Logger logger;
	private static Log log;
	
	private final static int LOG_TYPE_INFO = 1;
	private final static int LOG_TYPE_ERROR = 2;
	private final static int LOG_TYPE_WARNING = 3;

	public boolean logMessage(Log log, Set<LogLocation> logLocations, String typeLog) throws Exception {
		logger = Logger.getLogger("MyLog");
		this.log = log;
		String traceLog = ""; 
		int flagLog = 0; 
		boolean sucessfullLog = false; 
		
		validateLog(log);
		validateLogLocations(logLocations);
		
		if (log.getLevel().toString().equals(LevelLog.INFO.toString()) && typeLog.equals(LevelLog.INFO.toString())) {
			traceLog = traceLog + "error " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date())
					+" "+log.getMessage();
			flagLog = LOG_TYPE_INFO;
		} else if (log.getLevel().toString().equals(LevelLog.WARNING.toString()) && typeLog.equals(LevelLog.WARNING.toString())) {
			traceLog = traceLog + "warning " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date())
					+" "+log.getMessage();
			flagLog = LOG_TYPE_WARNING;
		} else {
			traceLog = traceLog + "error " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date())
					+" "+log.getMessage();
			flagLog = LOG_TYPE_ERROR;
		}	
		
		for(LogLocation it : logLocations) {
			sucessfullLog = putLoginIn(log, it, flagLog, traceLog);
		}
		return sucessfullLog;
	}
	
	private static boolean putLoginIn(Log log, LogLocation logLocation, int flagLog, String traceLog)
			throws Exception {
		if (logLocation.toString().equals(LogLocation.DATABASE.toString())) {
			Statement statement = DataSourceFactory.getConnection().createStatement();
			statement.executeUpdate("insert into Log_Values('" + traceLog + "', " + flagLog + ")");
			return true;
		} else if (logLocation.toString().equals(LogLocation.CONSOLE.toString())) {
			ConsoleHandler ch = new ConsoleHandler();
			logger.addHandler(ch);
			logger.log(log.getLevel(), "Log type: " + flagLog + " Message: " + log.getMessage() + " Trace log: " + traceLog);
			return true;
		} else if (logLocation.toString().equals(LogLocation.TEXT.toString())) {
			FileHandler fileHandler = KeepFile.createFile();
			logger.addHandler(fileHandler);
			logger.log(log.getLevel(), "Log type: " + flagLog + " Message: " + log.getMessage() + " Trace log: " + traceLog);			
			fileHandler.close();
			return true;
		} else
			return false;
	}
	
	private static void validateLog(Log log) throws Exception {
		if(log.getLevel().toString().isEmpty() || log.getMessage().isEmpty())
			throw new Exception("Error or Warning or Message must be specified");
	}
	
	private static void validateLogLocations(Set<LogLocation> logLocations) throws Exception {
		if(logLocations.isEmpty())
			throw new Exception("Invalid configuration");
	}
}
