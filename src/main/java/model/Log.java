package model;

import java.util.logging.Level;

public class Log {
	private String message;
	private Level level;
	
	public Log(String message, Level level) {
		super();
		this.message = message;
		this.level = level;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}	
}
