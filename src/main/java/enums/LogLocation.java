package enums;

public enum LogLocation {
	TEXT, 
	CONSOLE, 
	DATABASE;
}
