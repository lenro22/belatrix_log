package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.FileHandler;

public class KeepFile {

	private static String logFileFolder;
	
	public static FileHandler createFile(){
		loadConfigurations();
		FileHandler fileHandler = null;
		File logFile = new File(logFileFolder + "/logFile.txt");	
		try {
			fileHandler = new FileHandler(logFileFolder + "/logFile.txt");
			logFile.createNewFile();
			return fileHandler;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileHandler;
	}
	
	private static void loadConfigurations() {
		try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			logFileFolder = prop.getProperty("kp.logFileFolder");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
