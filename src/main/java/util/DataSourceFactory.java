package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataSourceFactory {

	private static String userDB;
	private static String passDB;
	private static String serverName;
	private static String dbms;
	private static String portNumber;

	public static Connection getConnection() throws SQLException {
		Connection connection = null;
		Properties connectionProps = new Properties();
		loadDBConfigurations();
		connectionProps.put("user", userDB);
		connectionProps.put("password", passDB);
		connection = DriverManager.getConnection("jdbc:" + dbms + "://" + serverName + ":" + portNumber + "/",
				connectionProps);
		return connection;
	}

	private static void loadDBConfigurations() {
		try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			userDB = prop.getProperty("db.user");
			passDB = prop.getProperty("db.pass");
			serverName = prop.getProperty("db.serverName");
			dbms = prop.getProperty("db.dbms");
			portNumber = prop.getProperty("db.portNumber");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
