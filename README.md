Revision de codigo:

1- Se crea un proyecto maven para adicionar las librerias que se van a utilizar en el transcurso del desarrollo, lo cual no estaba inicialmente definido en el proyecto.

2- Se descompone toda la logica de negocio contenida en JobLogger.class en modulos garantizando la seguridad de los objetos y sus atributos. Adicional, al tener el proyecto modularizado por funcionalidades especificas se da una mayor legibilidad y no se viola el principio de responsabilidad unica.

3- Se ajustan las firmas de los metodos con atributos mas genericos para permitir mayor legibilidad

4- Se crean dos clases utilitarias, una para separar la conexion a la base de datos y la otra para separar la escritura del archivo plano. 

5- Se garantiza que las conexiones se cierren una vez se hayan utilizado, cosa que no sucedia en el codigo inicial.

5- Habian demasiados condicionales repitiendo validaciones, se ajusto esto en los cuerpos de los metodos de nuestra clase inicial JobLogger.

6- Se tenia definido un String L que finalmente no cumplia con ninguna funcion, ya que no se estaba empleando. Se bautiza en el refactor como traceLog donde se define con mas claridad el objetivo de dicho atributo.

7- Se renombran los atributos, ya que inicialmente se utilizaban letras unicas lo cual no decian mucho del objetivo de su implementacion. Ahora tienen nombres mas claros de lo cual se puede llegar a intuir su funcion sin necesidad de leer todo el codigo para saber su objetivo.

8- Se elimina trim() del codigo y se utiliza .isEmpty() que valida que la cadena no contega valor. 

9- Se define un arreglo de tipo Set<LogLocation> esto con el fin de en una sola definicion tener las operaciones a realizar con log. Se utiliza la implementacion HashSet con el fin de no permitir elementos repetidos dentro del arreglo. 

10- Se crean enums para representar aquellas contantes que se podran utilizar genericamente en cualquier modulo del proyecto.

11- Se crea un archivo de propiedades las cuales son leidas para obtenerlos garantizando el ocultamiento y el acceso cuando sea necesario.

12- Se implementan tests para realizar pruebas basicas asegurando la consistencia y funcionalidad del aplicativo.